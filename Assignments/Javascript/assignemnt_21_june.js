// Assignemnt-1:script to convert number to currency using internationalization and localization of currency

var price= 48;

var currency= (new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }).format(price));

document.getElementById("assignemnt1").innerText= "Currency format for price " + price+ " is " + currency; 


//Assignemnt-2 : script to convert phone number to (xxx) xxx-xxxx format  using regular expression and replace method

var phone = "8557377655";
var phone1= phone.replace(/(\d{3})(\d{3})(\d{4})/, '($1) $2-$3');
document.getElementById("assignemnt2").innerText="Phone number " +phone+ " is converted to new format as " +phone1+ " using regulaer expression";
//document.write(phone+":"+phone1);

//Assignemnt-3: Day greeting

var today = new Date();
var hour = today.getHours();
var message;

if (hour >=0 & hour<12) {
        message='Good Morning';
        } else if (hour ==12) {
            message ='Good noon';
        
        } else if (hour >12 & hour<16)
        {
            message ="Good Afternoon";

        }else if(hour>16 & hour <19){
             message= "Good Evening";
    } 
else { message ="Good Night"}
document.getElementById("assignemnt3").innerText = message;
    
//Assignemnt 4: MonthName 
var day = today.getDay(); //var today is already declafred in asignemnt3
    const dayName=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
document.getElementById("assignemnt4").innerText = "Today is "+dayName[day];
 