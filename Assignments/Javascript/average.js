//Function to write average of the array

const priceList=[13.98,34.90,44,89.98,234.23,111.88]

function getAvg(priceList){
    let length= priceList.length;
    let index = 0;
    let sum =0;
    for (index; index < length; index++)
        sum+= priceList[index];

    return sum/length;

}

document.getElementById("avg").innerHTML=  +getAvg(priceList).toFixed(2);

// to count weekends

const schedule=["Jan 11 2018","Feb 11 2018","Mar 11 2018","Apr 11 1018","May 11 2018","Jun 11 2018","Jul 11 2018","Aug 11 2018","Sep 11 2018","Oct 11 2018","Nov 11 2018","Dec 11 2018"];

function countWeekend(dates){
    let index=0;
    let length=dates.length;
    let count=0;
    let date;
    for (index; index < length; index++) {
         date = new Date(dates[index])
        if((date.getDay()==0 ) || (date.getDay()==6))
            count+=1;
              }
     return count;

}

document.getElementById("weekend").innerHTML="Number of weekend:" + countWeekend(schedule);
